/**
 * Created by ksheppard on 19/07/2016.
 */

var gulp = require('gulp'),
    less = require('gulp-less'),
    rename = require('gulp-rename'),
    cleanCss = require('gulp-clean-css'),
    sourcemaps = require('gulp-sourcemaps'),
    prefix = require('gulp-autoprefixer');

var assets = {
    css: {
        base: 'src'
    },
    dist: {
        path: 'dist'
    },
    docs: {
        path: '../basecoat-docs'
    }
};

assets.css.less = [
    assets.css.base + '/basecoat.less'
]; //or '/**/[^_]*.less' to exclude partials

assets.dist.files = [
    assets.dist.path + '/basecoat.*',
    assets.dist.path + '/sprites/**/*.*'
];

/* ****** Distribution Block ***** */

gulp.task('less', function() {
    return gulp.src(assets.css.less)
        .pipe(sourcemaps.init())
        .pipe(less().on('error', console.log))
        .pipe(prefix({browsers: ['last 2 versions']}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(assets.dist.path));
});

gulp.task('minify-css', ['less'], function () {
    return gulp.src(assets.dist.path + '/basecoat.css')
        .pipe(cleanCss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(assets.dist.path));
});

gulp.task('docs', ['minify-css'], function () {
    return gulp.src(assets.dist.files, {base: assets.dist.path})
        .pipe(gulp.dest(assets.docs.path + '/css/basecoat'));
});

/* ****** Debug Block ****** */

/* Watches all the *.less and *.css files for changes and runs the concat-css task */
gulp.task('watch-css', ['less'], function() {
    gulp.watch(assets.css.base + "/**/*.less", ['less']);
});